/**
 * サンプル02　記憶する
 */
var restify = require('restify');
var builder = require('botbuilder');

// HTTPサーバ(RESTサーバ)をセットアップ
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url); 
});
  
// Bot Framework Service への接続を作成
var connector = new builder.ChatConnector({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
    openIdMetadata: process.env.BotOpenIdMetadata
});
// サーバーのパスにボットを接続
server.post('/api/messages', connector.listen());

// 会話プログラム本体
const bot = new builder.UniversalBot(connector, [
    // ここに記載されたものが最初の会話になります。
    (session, args, next) => {
        if(session.privateConversationData.response){
            session.send(`覚えていますよ。「${session.privateConversationData.response}」`);
        }
        session.privateConversationData.response = session.message.text;
        session.send(`あなたが言ったことを覚えます。`);
    }
]);
