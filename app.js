/**
 * サンプルアプリケーション
 */
var restify = require('restify');
var builder = require('botbuilder');

// HTTPサーバ(RESTサーバ)をセットアップ
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url); 
});
  
// Bot Framework Service への接続を作成
var connector = new builder.ChatConnector({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
    openIdMetadata: process.env.BotOpenIdMetadata
});

// サーバーのパスにボットを接続
server.post('/api/messages', connector.listen());

//
//
// endConversation 会話セッションを終了する

const bot = new builder.UniversalBot(connector, [
    // ここに記載されたものが最初の会話になります。
    (session, args, next) => {
        // 初めてメッセージを受け取った時に動きます。
        session.send(`${session.message.address.user.name} on ${session.message.address.channelId.name}. 初めて話しかけてくれたのですね。`);
        session.send(`あなたはこう言いました「${session.message.text}」`);
        session.beginDialog('firstDialog');
    },
    (session, results, next) => {
        // beginDialogで開始した会話が終了した
        // 回答をチェック
        if (results.response) {
            // 回答をprivateConversationDataに保存することができます。
            const firstResponse = session.privateConversationData.firstResponse = results.response;
            session.beginDialog('secondDialog');
        } else {
            // 正しい回答を得られなかった場合
            session.endConversation(`残念ながら回答を得ることはできませんでした。`);
        }
    },
    (session, results, next) => {
        // 二つ目の質問の回答
        if (results.response) {
            const firstResponse = session.privateConversationData.firstResponse;
            const secondResponse = session.privateConversationData.secondResponse = results.secondResponse;
            session.endConversation(`最初の回答「${firstResponse}」、二つ目の回答「${secondResponse}」でした。\n終了です。`);
        } else {
            // 正しい回答を得られなかった場合
            session.endConversation(`残念ながら回答を得ることはできませんでした。`);
        }
    },
]);

//ダイアログ(会話)
bot.dialog('firstDialog', [
    (session, args, next) => {
        // 会話の最初です。
        // args には引数が入ります。
        if(args && args.isReprompt){
            session.dialogData.isReprompt = args.isReprompt;
            session.dialogData.repromptCount = args.repromptCount;
            builder.Prompts.text(session, "もう一度聞きます");
        }else{
            // 質問を表示
            builder.Prompts.text(session, '最初の質問です。答えを入力すると次に進みます。');
        }
    },
    (session, results, next) => {
        // 会話の返事
        const answer = results.response;
        if(answer.trim().length <= 3){
            if(session.dialogData.isReprompt && session.dialogData.repromptCount >= 3){
                session.send(`${session.dialogData.repromptCount}回やってもうまくいかない`);
                session.send(`残念ながらもう限界です`);
                session.endDialogWithResult({ response: '' });
            }else{
                session.send('もう少し長い答えが欲しいです');
                repromptCount = 0;
                if(session.dialogData.repromptCount){
                    repromptCount = session.dialogData.repromptCount;
                }
                session.replaceDialog('firstDialog', { isReprompt: true,
                                                       repromptCount: repromptCount+1});
            }
        }else{
            session.endDialogWithResult({ response: answer.trim() });
        }
    }
]);


//ダイアログ(会話)
bot.dialog('secondDialog', [
    (session, args, next) => {
        // 会話の最初です。
        // args には引数が入ります。
        if(args && args.isReprompt){
            session.dialogData.isReprompt = args.isReprompt;
            session.dialogData.repromptCount = args.repromptCount;
            builder.Prompts.text(session, "もう一度聞きます");
        }else{
            // 質問を表示
            builder.Prompts.text(session, '二つ目の質問です。答えを入力すると次に進みます。');
        }
    },
    (session, results, next) => {
        // 会話の返事
        const answer = results.response;
        if(answer.trim().length <= 3){
            if(session.dialogData.isReprompt && session.dialogData.repromptCount >= 3){
                session.send(`${session.dialogData.repromptCount}回やってもうまくいかない`);
                session.send(`残念ながらもう限界です`);
                session.endDialogWithResult({ response: '' });
            }else{
                session.send('もう少し長い答えが欲しいです');
                repromptCount = 0;
                if(session.dialogData.repromptCount){
                    repromptCount = session.dialogData.repromptCount;
                }
                session.replaceDialog('secondDialog', { isReprompt: true,
                                                       repromptCount: repromptCount+1});
            }
        }else{
            session.endDialogWithResult({ response: answer.trim() });
        }
    }
]);
